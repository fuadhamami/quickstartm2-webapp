from webapp import db
from webapp.models import Mantan, User

# drop all of the existing database table
db.drop_all()

#create the database and the database table
db.create_all()

# insert data
mantan1 = Mantan('haruka', 'gak bisa bahsa jepang')
mantan2 = Mantan('franda', 'ditinggal nikah duluan')
mantan3 = Mantan('raisa', 'konser mulu gak pernah ketemuan')
db.session.add(mantan1)
db.session.add(mantan2)
db.session.add(mantan3)

# insert user data
user1 = User('qwe@us.er', 'qweqwe')
user2 = User('asd@us.er', 'asdasd')
user3 = User('zxc@us.er', 'zxczxc')
db.session.add(user1)
db.session.add(user2)
db.session.add(user3)

admin_user = User(email='admin@web.app', plaintext_password='admin', role='admin')
db.session.add(admin_user)

#commit the changes
db.session.commit()

