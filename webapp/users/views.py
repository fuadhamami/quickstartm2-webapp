from flask import render_template, Blueprint, request, flash, url_for, redirect
from .form import RegisterForm, LoginForm, EmailForm, PasswordForm
from sqlalchemy.exc import IntegrityError
from flask_login import login_required, login_user, logout_user, current_user
from datetime import datetime

from webapp.models import User
from webapp import db

users_blueprint = Blueprint('users', __name__)

@users_blueprint.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm(request.form)
	if request.method == 'POST':
		if form.validate_on_submit():
			user = User.query.filter_by(email=form.email.data).first()
			if user is not None and user.is_correct_password(form.password.data):
				user.authenticated = True
				user.last_logged_in = user.current_logged_in
				user.current_logged_in = datetime.now()
				db.session.add(user)
				db.session.commit()
				login_user(user)
				flash('Thanks for logging in, {}'.format(current_user.email))
				return redirect('/')
			else:
				flash('ERROR! Incorrect login credentials.', 'error')
	return render_template('login.html', form=form)

@users_blueprint.route('/register', methods=['GET', 'POST'])
def register():
	form = RegisterForm(request.form)
	if request.method == 'POST':
		if form.validate_on_submit():
			try:
				new_user = User(form.email.data, form.password.data)
				new_user.authenticated = True
				db.session.add(new_user)
				db.session.commit()
				login_user(new_user)

				flash('Thanks for registering!', 'success')
				return redirect('/')
			except IntegrityError:
				db.session.rollback()
				flash('ERROR! Email ({}) already exists.'.format(form.email.data), 'error')
	return render_template('register.html', form=form)

@users_blueprint.route('/logout')
@login_required
def logout():
	user = current_user
	user.authenticated = False
	db.session.add(user)
	db.session.commit()
	logout_user()
	flash('Goodbye!', 'info')
	return redirect(url_for('users.login'))

@users_blueprint.route('/user_profile')
@login_required
def user_profile():
	return render_template('user_profile.html')

@users_blueprint.route('/email_change', methods=["GET", "POST"])
@login_required
def user_email_change():
	form = EmailForm()
	if request.method == 'POST':
		if form.validate_on_submit():
			try:
				user_check = User.query.filter_by(email=form.email.data).first()
				if user_check is None:
					user = current_user
					user.email = form.email.data
					db.session.add(user)
					db.session.commit()
					flash('Email changed!')
					return redirect('/')
				else:
					flash('Sorry, that email already exist', 'error')
			except IntegrityError:
				flash('Sorry, that email already exist', 'error')
	return render_template('email_change.html', form=form)

@users_blueprint.route('/password_change', methods=["GET", "POST"])
@login_required
def user_password_change():
	form = PasswordForm()
	if request.method == 'POST':
		if form.validate_on_submit():
			user = current_user
			user.password = form.password.data
			db.session.add(user)
			db.session.commit()
			flash('Password has been updated!', 'success')
			return redirect(url_for('users.user_profile'))

	return render_template('password_change.html', form=form)


@users_blueprint.route('/admin')
@login_required
def admin():
	pass