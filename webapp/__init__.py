from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bcrypt import Bcrypt
from flask_debugtoolbar import DebugToolbarExtension

app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('flask.cfg')

# app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

DEBUG_TB_INTERCEPT_REDIRECTS = False
toolbar = DebugToolbarExtension(app)

db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
# mail = Mail(app)

# flask login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "users.login"

from webapp.models import User, Mantan
from webapp import admin

@login_manager.user_loader
def load_user(user_id):
	return User.query.filter(User.id == int(user_id)).first()

# from . import views
from webapp.users.views import users_blueprint
from webapp.kode.views import kode_blueprint

# register the blueprint
app.register_blueprint(users_blueprint)
app.register_blueprint(kode_blueprint)