from flask_admin.contrib.sqla import ModelView
from flask_admin import Admin
from flask import session, abort

from webapp.models import db, Mantan, User
from webapp import app

from flask_login import current_user

class WebappModelView(ModelView):

	def is_accessible(self):
		if current_user.role != 'admin':
			return abort(403)
		else:
			return True

	def inaccessible_callback(self, name, **kwargs):
		# redirect to login page if user doesn't have access
		return redirect(url_for('login', next=request.url))

# class UserView(ModelView):
#         can_delete = False  # disable model deletion

# class RecipeView(ModelView):
#         page_size = 50  # the number of entries to display on the list view

admin = Admin(app, name='webapp', template_mode='bootstrap3')
# admin.add_view(UserView(User, db.session))
# admin.add_view(RecipeView(Recipe, db.session))

admin.add_view(WebappModelView(User, db.session))
admin.add_view(WebappModelView(Mantan, db.session))