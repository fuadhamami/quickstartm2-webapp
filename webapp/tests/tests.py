import unittest

from webapp import app

class WebappTests(unittest.TestCase):

	# setup and teardown

	# executed prior to each test
	def setUp(self):
		app.config['TESTING'] = True
		app.config['DEBUG'] = True
		self.app = app.test_client()

		self.assertEquals(app.debug, True)

	# executed after each test
	def tearDown(self):
		pass

	# tests
	def test_main_page(self):
		response = self.app.get('/', follow_redirects=True)
		self.assertIn(b'Kennedy Family Recipes', response.data)
		self.assertIn(b'add', response.data)
		self.assertIn(b'Lunch Recipes', response.data)
		self.assertIn(b'Dinner Recipes', response.data)
		self.assertIn(b'Dessert Recipes', response.data)
		self.assertIn(b'Slow-Cooker Tacos', response.data)

if __name__ == "__main__":
	unittest.main()