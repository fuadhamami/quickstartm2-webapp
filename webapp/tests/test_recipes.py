import os
import unittest

from webapp import app, db

TEST_DB = 'test.db'

class WebappTest(unittest.TestCase):
	#setup and teardown
	
	#execute prior to each test
	def setUp(self):
		app.config['TESTING'] = True
		app.config['WTF_CSRF_ENABLED'] = False
		app.config['DEBUG'] = False
		app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(app.config['BASEDIR'], TEST_DB)
		self.app = app.test_client()
		db.create_all()

		self.assertEqual(app.debug, False)

	#executed after each test
	def tearDown(self):
		db.session.remove()
		db.drop_all()

	# tests

	def test_main_page(self):
		response = self.app.get('/', follow_redirects=True)
		self.assertIn(b'add', response.data)
		self.assertIn(b'Register', response.data)
		self.assertIn(b'Log In', response.data)
		self.assertIn(b'Log Out', response.data)

	def test_main_page_query_results(self):
		response = self.app.get('/add', follow_redirects=True)
		self.assertIn(b'Add', response.data)

	def test_add_recipe(self):
		response = self.app.post('/add', data=dict(recipe_title='Hamburgers', recipe_description='Delicious hamburger'), follow_redirects=True)

	def test_add_invalid_recipes(self):
		response = self.app.post('/add', data=dict(recipe_title='', recipe_description='Delicious hamburger'), follow_redirects=True)
		self.assertIn(b'ERROR! Recipe was not added.', response.data)
		self.assertIn(b'This field is required.', response.data)

	def login(self, email, password):
    return self.app.post(
        '/login',
        data=dict(email=email, password=password),
        follow_redirects=True
    )

def test_login_form_displays(self):
    response = self.app.get('/login')
    self.assertEqual(response.status_code, 200)
    self.assertIn(b'Log In', response.data)
 
def test_valid_login(self):
    self.app.get('/register', follow_redirects=True)
    self.register('patkennedy79@gmail.com', 'FlaskIsAwesome', 'FlaskIsAwesome')
    self.app.get('/login', follow_redirects=True)
    response = self.login('patkennedy79@gmail.com', 'FlaskIsAwesome')
    self.assertIn(b'Welcome, patkennedy79@gmail.com!', response.data)
 
def test_login_without_registering(self):
    self.app.get('/login', follow_redirects=True)
    response = self.login('patkennedy79@gmail.com', 'FlaskIsAwesome')
    self.assertIn(b'ERROR! Incorrect login credentials.', response.data)
 
def test_valid_logout(self):
    self.app.get('/register', follow_redirects=True)
    self.register('patkennedy79@gmail.com', 'FlaskIsAwesome', 'FlaskIsAwesome')
    self.app.get('/login', follow_redirects=True)
    self.login('patkennedy79@gmail.com', 'FlaskIsAwesome')
    response = self.app.get('/logout', follow_redirects=True)
    self.assertIn(b'Goodbye!', response.data)
 
def test_invalid_logout_within_being_logged_in(self):
    response = self.app.get('/logout', follow_redirects=True)
    self.assertIn(b'Log In', response.data)

if __name__ == "__main__":
	unittest.main()
