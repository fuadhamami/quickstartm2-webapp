from flask import render_template, Blueprint, request, redirect, flash, url_for
from webapp.models import Mantan

from .forms import AddMantanForm
from webapp import db

kode_blueprint = Blueprint('kode', __name__, template_folder='templates')

@kode_blueprint.route('/')
def index():
	all_mantans = Mantan.query.all()
	return render_template('mantans.html',
							mantans=all_mantans)

@kode_blueprint.route('/add', methods=['GET', 'POST'])
def add_mantan():
	form = AddMantanForm(request.form)
	if request.method == 'POST':
		if form.validate_on_submit():
			new_mantan = Mantan(form.nama_mantan.data, form.alasan_putus.data)
			db.session.add(new_mantan)
			db.session.commit()
			flash('New mantan, {}, added!'.format(new_mantan.nama_mantan), 'success')
			return redirect(url_for('kode.index'))
		else:
			# flash_error(form)
			flash('ERROR! Mantan was not added.', 'error')
	return render_template('add_mantan.html',
							form=form)