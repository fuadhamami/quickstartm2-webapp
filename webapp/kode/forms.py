from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import DataRequired

class AddMantanForm(Form):
	nama_mantan = StringField('Nama Mantan', validators=[DataRequired()])
	alasan_putus = StringField('Alasan Putus', validators=[DataRequired()])