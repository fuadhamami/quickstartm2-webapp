## POSTGRESQL DATABASE 

Creating a New User and New Database in PostgreSQL

	CREATE USER webapp WITH PASSWORD 'gedang';

check pg_user system catalog

	SELECT usename FROM pg_user;

create a new database

	CREATE DATABASE webapp_db owner webapp;

To check that the database exists, use the '\l' command to list all the databases 

	\l

connect to the 'webapp_db' database:

	\c webapp_db

get a listing of all the tables in the database using the '\d' command

	\d

check that the three mantans that we added in 'db_create.py' are actually in the database using the following query:

	SELECT * FROM mantans;

You’ll have to enter ‘\q’ to exit the psql shell

	\q

to delete database with psql shell

	DROP DATABASE db_name;



## OPENSHIFT

PostgreSQL 9.2 database added.  Please make note of these credentials:

   Root User: adminuzaplrm
   Root Password: sz8kS1fGQv8P
   Database Name: webapp

Connection URL: postgresql://$OPENSHIFT_POSTGRESQL_DB_HOST:$OPENSHIFT_POSTGRESQL_DB_PORT


