from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from webapp import app, db

# app.config.from_pyfile('flask.cfg')

manager = Manager(app)
migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)

if __name__ == "__main__":
	manager.run()